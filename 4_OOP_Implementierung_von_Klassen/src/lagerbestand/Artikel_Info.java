package lagerbestand;

public class Artikel_Info {
	
	//Attribute
	private int artikelnummer;
	private String bezeichnung;
	private double einkaufspreis;
	private double verkaufspreis;
	private int lagerbestand_soll;
	private int lagerbestand_ist;
	
	//methoden
	public void set_artikelnummer(int artikelnummer) 
	  	{ 
	    this.artikelnummer = artikelnummer; 
	  	} 
		public int get_artikelnummer() 
	  	{ 
	  	return this.artikelnummer; 
	  	}
	  	
	public void set_bezeicnung(String bezeichnung) 
		{ 
	    this.bezeichnung = bezeichnung; 
		} 
		public String get_bezeichnung() 
		{ 
		return this.bezeichnung; 
		}
		
	public void set_einkaufspreis(double einkaufspreis) 
		{
		this.einkaufspreis = einkaufspreis; 
		} 
		public double get_einkaufspreis() 
		{ 
		return this.einkaufspreis; 
		}

	public void set_verkaufspreis(double verkaufspreis) 
		{
		this.verkaufspreis = verkaufspreis ; 
		} 
		public double get_verkauspreis() 
		{ 
		return this.verkaufspreis; 
		}

	public void set_alagerbestand_soll(int lagerbestand_soll) 
	  	{ 
	    this.lagerbestand_soll = lagerbestand_soll; 
	  	} 
	  	public int get_lagerbestand_soll() 
	  	{ 
	    return this.lagerbestand_soll; 
	    }

	public void set_lagerbestand_ist(int lagerbestand_ist) 
	  	{ 
	    this.lagerbestand_ist = lagerbestand_ist; 
	  	} 
	  	public int get_lagerbestand_ist() 
	  	{ 
	    return this.lagerbestand_ist; 
	    }
}