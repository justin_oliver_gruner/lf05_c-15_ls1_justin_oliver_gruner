package raumschiff;

import java.util.ArrayList;

public class A_4_1 {

	public static void main(String[] args) {
		
		/* Aufbauschema f�r die Konstruktoren
		 * 
		 * Ladung: name menge
		 * A_4_1:  PhotonentorpedoAnzal , EnergieversorgungInProzent , SchildeInProzent
		 *         HuelleInProzent , LebenserhaltungssystemeInProzent , AndroidenAnzahl , Schiffsname
		 */
		
		//klingonen
		Ladung lk1 = new Ladung ("Ferengi Schnekensaft",200);
		A_4_1 rk1 = new A_4_1(0,100,100,100,100,2,"IKS Hegh'ta");
		Ladung lk2 = new Ladung("Bat'leth Klingonen Schwert",200);
		
		//romulaner
		Ladung lr1 = new Ladung ("Borg-Schrott",5);
		Ladung lr2 = new Ladung ("Rote Materie",2);
		A_4_1 rr1 = new A_4_1(2,100,100,100,100,2,"IRW Khazara");
		Ladung lr3 = new Ladung("Plasma-Waffe",50);
	    
	    //vulkanier
	    Ladung lv1 = new Ladung ("Forschungssonde",35);
		A_4_1 rv1 = new A_4_1(0,80,80,50,100,5,"Ni'Var");
		Ladung lv2 = new Ladung("Phodonentorpedo",3);
		
		System.out.println(rk1);
		System.out.println(rr1);
		System.out.println(rv1);
		
	}
	
	public String toString()
	{
		return this.PhotonentorpedoAnzal + " " + this.EnergieversorgungInProzent + "% " + this.SchildeInProzent + "% " + this.HuelleInProzent + "% " + this.LebenserhaltungssystemeInProzent + "% " + this.AndroidenAnzahl  + " Name: " + this.Schiffsname ;
	}
	//Attribute	
	private int PhotonentorpedoAnzal;
	private int EnergieversorgungInProzent;
	private int SchildeInProzent;
	private int HuelleInProzent;
	private int LebenserhaltungssystemeInProzent;
	private int AndroidenAnzahl;
	private String Schiffsname;
		
		public A_4_1(int PhotonentorpedoAnzal, int EnergieversorgungInProzent, int SchildeInProzent,
		int HuelleInProzent, int LebenserhaltungssystemeInProzent, int AndroidenAnzahl, String Schiffsname)
		{
			set_PhotonentorpedoAnzal(PhotonentorpedoAnzal);
			set_EnergieversorgungInProzent(EnergieversorgungInProzent);
			set_SchildeInProzent(SchildeInProzent);
			set_HuelleInProzent(HuelleInProzent);
			set_LebenserhaltungssystemeInProzent(LebenserhaltungssystemeInProzent);
			set_AndroidenAnzahl(AndroidenAnzahl);
			set_Schiffsname(Schiffsname);
		}
		
		//Methoden
		public void set_PhotonentorpedoAnzal (int PhotonentorpedoAnzal) 
		{ 
	    this.PhotonentorpedoAnzal = PhotonentorpedoAnzal; 
	    } 
		public int get_PhotonentorpedoAnzal()
		{
	  	return this.PhotonentorpedoAnzal; 
	  	}
		
		public void set_EnergieversorgungInProzent (int EnergieversorgungInProzent) 
		{ 
	    this.EnergieversorgungInProzent = EnergieversorgungInProzent; 
	    } 
		public int get_EnergieversorgungInProzent()
		{
	  	return this.EnergieversorgungInProzent; 
	  	}
		
		public void set_SchildeInProzent (int SchildeInProzent) 
		{ 
	    this.SchildeInProzent = SchildeInProzent; 
	    } 
		public int get_SchildeInProzent()
		{
	  	return this.SchildeInProzent; 
	  	}
		
		public void set_HuelleInProzent (int HuelleInProzent) 
		{ 
	    this.HuelleInProzent = HuelleInProzent; 
	    } 
		public int get_HuelleInProzent()
		{
	  	return this.HuelleInProzent; 
	  	}
		
		public void set_LebenserhaltungssystemeInProzent (int LebenserhaltungssystemeInProzent) 
		{ 
	    this.LebenserhaltungssystemeInProzent = LebenserhaltungssystemeInProzent; 
	    } 
		public int get_LebenserhaltungssystemeInProzent()
		{
	  	return this.LebenserhaltungssystemeInProzent; 
	  	}
		
		public void set_AndroidenAnzahl (int AndroidenAnzahl) 
		{ 
	    this.AndroidenAnzahl = AndroidenAnzahl; 
	    } 
		public int get_AndroidenAnzahl()
		{
	  	return this.AndroidenAnzahl; 
	  	}
		
		public void set_Schiffsname (String Schiffsname) 
		{ 
	    this.Schiffsname = Schiffsname; 
	    } 
		public String get_Schiffsname()
		{
	  	return this.Schiffsname; 
	  	}
}