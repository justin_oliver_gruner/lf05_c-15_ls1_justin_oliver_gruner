import java.util.Arrays;

public class A_5_2__Lotto {

	public static void main(String[] args) {
		
		//Eingabe der zu �berpr�fenden zahlen
		int zahlChek1 = 12;
		int zahlChek2 = 13;
		
		// Array Zahlen Eingabe
		int arr[] = { 3, 7, 12, 18, 37, 42};
		
		// Ausgabe des Arrays in der Konsole
		System.out.println("Array: " + Arrays.toString(arr));
		
		// �berpr�fung ob die Zahl im Array vorhanden ist
		pr�fe1(arr, zahlChek1);
		pr�fe2(arr, zahlChek2);
		
	}
	
	private static void pr�fe1(int[] arr, int zahlChek1) {
		
		int zahl1;
		int c = 0;
		for (int i = 0; i < arr.length; i++) {			
			zahl1 = arr[i];
			if (zahl1 == zahlChek1) {
				System.out.println("Die Zahl " + zahlChek1 + " ist in der Ziehung enthalten");	
			}
			else {	c+=1;	}
			if  (c > 5) {
				System.out.println("Die Zahl " + zahlChek1 + " ist nicht in der Ziehung enthalten");
			}
		}
	}
	
	private static void pr�fe2(int[] arr, int zahlChek2) {
		
		int zahl2;
		int c = 0;
		for (int i = 0; i < arr.length; i++) {			
			zahl2 = arr[i];
			if (zahl2 == zahlChek2 ) {
				System.out.println("Die Zahl " + zahlChek2 + " ist in der Ziehung enthalten");	
			}
			else {	c+=1;	}
			if  (c > 5) {
				System.out.println("Die Zahl " + zahlChek2 + " ist nicht in der Ziehung enthalten");
			}
		}
	}
}