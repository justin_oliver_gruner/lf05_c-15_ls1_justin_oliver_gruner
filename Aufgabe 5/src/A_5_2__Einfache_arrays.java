
public class A_5_2__Einfache_arrays {

	public static void main(String[] args) {
		
		int laenge = 10;
		double[] zahl = new double[laenge];
		
		for (int i = 0;i < laenge;i++) {
			zahl[i] = i;
		}
		
		for (int index = 0; index < zahl.length; index++) {
			System.out.println(zahl[index]);
		}
	}

}
