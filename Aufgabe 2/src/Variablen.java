
public class Variablen {

	public static void main(String[] args) {
		
		for(int i = 0; i < 10/2; i++){

		     System.out.print(i);
		}
		
		//1
		int Programmdurchlaeufe;
		
		//2
		Programmdurchlaeufe = 25;
		System.out.println("Programmdurchlaeufe: " + Programmdurchlaeufe);
		
		//3
		String Menuepunkt;
		
		//4
		Menuepunkt = "c" ;
		System.out.println("Menuepunkt: " + Menuepunkt);
		
		//5
		long Lichtgeschwindigkeit;
		
		//6
		Lichtgeschwindigkeit = 299792458;
		System.out.println("Lichtgeschwindigkeit: " + Lichtgeschwindigkeit + "ms");
		
		//7
		byte Vereinsverwaltung = 7;
		
		//8
		System.out.println("Anzahl der Mitglieder: " + Vereinsverwaltung);
		
		//9
		double Elementarladung = 1.602176634*10-19;
		System.out.println("Elementarladung: " + Elementarladung + "As");
		
		//10
		boolean Zahlung_erfolgt;
		
		//11
		Zahlung_erfolgt = true;
		System.out.println("Zahlung erfolgt? " + Zahlung_erfolgt);

	}

}
