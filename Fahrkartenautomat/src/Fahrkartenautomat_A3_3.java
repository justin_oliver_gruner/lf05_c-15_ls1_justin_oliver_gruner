import java.util.Scanner;

public class Fahrkartenautomat_A3_3 {
	
	public static double fahrkartenbestellungErfassen() {
		
		double einzelTicketPreis;
		double ticketAnzahl;
		double zuZahlenderBetrag;
		
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Ticketpreis (EURO): ");
	    einzelTicketPreis = tastatur.nextDouble();
	    
	    System.out.print("Anzahl der Tikets");
	    ticketAnzahl = tastatur.nextDouble();
	    return zuZahlenderBetrag = einzelTicketPreis * ticketAnzahl;
		
	}
	
	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		
		double eingezahlterGesamtbetrag;
		double eingeworfeneM�nze;
		double z = 0;
		Scanner tastatur = new Scanner(System.in);
		
		eingezahlterGesamtbetrag = 0.00;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	    	   eingezahlterGesamtbetrag += eingeworfeneM�nze;
	       }
	       return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
		
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		
		double r�ckgabebetrag;
		
		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       if(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + "%.2f\n" + " EURO");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 2.0;
	           }
	           while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 1.0;
	           }
	           while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 0.5;
	           }
	           while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 0.2;
	           }
	           while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 0.1;
	           }
	           while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 0.05;
	           }
	      }
	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                   "vor Fahrtantritt entwerten zu lassen!\n"+
                   "Wir w�nschen Ihnen eine gute Fahrt.");
	}
	
	public static void main(String[] args)
    {
        double x;
        double y;
        
        x=fahrkartenbestellungErfassen();
        y=fahrkartenBezahlen(x);
        fahrkartenAusgeben();
        rueckgeldAusgeben(y,x);

    }
}