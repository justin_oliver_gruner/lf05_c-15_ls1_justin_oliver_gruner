
                      public class Aufgabe_3 {

	public static void main(String[] args) {
		
		double c1=-28.8889, c2=-23.3333, c3=-17.7778, c4=-6.6667, c5=-1.1111;

		c1=runde(c1);
		c2=runde(c2);
		c3=runde(c3);
		c4=runde(c4);
		c5=runde(c5);
		
		System.out.printf("%-12s|", "Fahrenheit"); System.out.printf("%10s\n", "Celsius");
		
		for(int i=23 ; i>0; i--) {
			System.out.print("-");
		}
		
		System.out.printf("\n%-12s|", "-20"); System.out.printf("%10s", c1);		
		System.out.printf("\n%-12s|", "-10"); System.out.printf("%10s", c2);		
		System.out.printf("\n%-12s|", "+"+"0"); System.out.printf("%10s", c3);		
		System.out.printf("\n%-12s|", "+"+"20"); System.out.printf("%10s", c4);		
		System.out.printf("\n%-12s|", "+"+"30"); System.out.printf("%10s", c5);
	}
	
	public static double runde(double i) {
		
		double summe = Math.round(i*100)/100d;
		return summe;
	}

}