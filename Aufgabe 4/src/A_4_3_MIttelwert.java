import java.util.Scanner;

public class A_4_3_MIttelwert {

	public static void main(String[] args) {	
		
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("eingabe zahlen anzahl");
		int anzahl = tastatur.nextInt();
		double summe = 0;
		int zaehler = 0;
		while(zaehler < anzahl) {
			System.out.println("eingabe einer zahl");
			double y = tastatur.nextDouble();
			summe = summe + y;
			zaehler ++;
		}
		
		double m = summe / anzahl;
		
		System.out.printf("Der Mittelwert betragt %.2f\n", m);
	}
}