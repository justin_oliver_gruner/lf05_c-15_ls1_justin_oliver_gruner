
public class A_4_4_Wettlauf {

	public static void main(String[] args) {
		
		float distanz_laeuferA = 0;
		int distanz_laeuferB = 250;
		int dauer = 0;
		System.out.printf("%-20s","A = " + "0" + "m in " + "0" + "s"); System.out.printf(" | ");
		System.out.printf("%-20s","B = " + "0" + "m in " + "0" + "s"); System.out.printf("|");
		System.out.println("\n--------------------------------------------");
		
		while(distanz_laeuferA < 1000 && distanz_laeuferB < 1000) {
			
			distanz_laeuferB = distanz_laeuferB + 7;
			distanz_laeuferA = distanz_laeuferA + 9.5f;
			dauer ++;
			
			System.out.printf("%-20s","A = " + distanz_laeuferA + "m in " + dauer + "s"); System.out.printf(" | "); 
			
			System.out.printf("%-20s","B = " + distanz_laeuferB + "m in " + dauer + "s"); System.out.printf("|");
			System.out.print("\n--------------------------------------------\n");
		}
	}
}